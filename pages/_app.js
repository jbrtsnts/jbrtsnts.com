import '@styles/tailwind.css';
import '@styles/index.scss';

import Head from 'next/head';
import React, { useEffect } from 'react';

function MyApp({ Component, pageProps }) {

  useEffect(() => {
    startCursorInteraction();
  });

  let startCursorInteraction = async () => {
    const animatedCursor = await import('../plugins/animated-cursor/index').then(module => module.default);
    // you can now use the package in here
    const Cursor = new animatedCursor(document.querySelector('.animated-cursor'));

    [...document.querySelectorAll('a')].forEach(el => {
      el.addEventListener('mouseenter', () => Cursor.emit('enter'));
      el.addEventListener('mouseleave', () => Cursor.emit('leave'));
    });
  }
  

  return <main>
    <Head>
      <title>jbrtsnts</title>
      <meta charSet="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    </Head>
    <h1 className="site-name">jbrtsnts</h1>
    <Component {...pageProps} />
    <svg className="animated-cursor" width="80" height="80" viewBox="0 0 220 220">
			<defs>
				<filter id="filter-1" x="-50%" y="-50%" width="200%" height="200%" 
				filterUnits="objectBoundingBox">
					<feTurbulence type="fractalNoise" baseFrequency="0" numOctaves="1" result="warp" />
					<feOffset dx="-30" result="warpOffset" />
					<feDisplacementMap xChannelSelector="R" yChannelSelector="G" scale="30" in="SourceGraphic" in2="warpOffset" />
				</filter>
			</defs>
			<circle className="cursor__inner" cx="110" cy="110" r="60"/>
		</svg>
  </main>
}

export default MyApp
