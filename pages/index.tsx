import React, { useEffect } from 'react';
import Particle from '../plugins/particles/App.js';``
export default function Home() {
  useEffect(() => {
    startParticlePortfolio();
  });

  const startParticlePortfolio = async () => {
    // const Particle = await import('../plugins/particles/App.js').then(module => module.default); // plugins/particles/App.js
    const profile = new Particle();
    profile.init()
  }

  return (
    <section>
      <div className="container"></div>
    </section>
  );
}
