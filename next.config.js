const path = require('path');
const __root = path.resolve(__dirname, '.');

module.exports = {
  webpack: (config, { buildId, dev, isServer, defaultLoaders, webpack }) => {
    // Note: we provide webpack above so you should not `require` it
    // Perform customizations to webpack config
    // config.plugins.push(new webpack.IgnorePlugin(/\/__tests__\//))

    config.module.rules.push({
      test: /\.(glsl|frag|vert)$/,
      use: ['glslify-import-loader', 'raw-loader', 'glslify-loader']
    })

    config.module.rules.push({
      test: /three\/examples\/js/,
      use: 'imports-loader?THREE=three'
    })

    config.resolve.alias['three-examples'] = path.join(__root, './node_modules/three/examples/js')
    config.plugins.push(
      new webpack.ProvidePlugin({
        'THREE': 'three'
      })
    )
    // Important: return the modified config
    return config
  },
}